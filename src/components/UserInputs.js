import React from "react";

const UserInputs = (props) => {
  //Functions to allow users to press enter to submit inputs
  function enterNameInput(e) {
    if (e.keyCode == 13) {
      return props.submitNameInput();
    }
  }

  function enterDataInput(e) {
    if (e.keyCode == 13) {
      return props.submitRawGraphInput();
    }
  }

  return (
    <div>
      <h1>Chart Generator</h1>
      <div className="form-inputs">
        {props.formattedLineGInput !== "" && props.graphType == "" ? (
          <div className="error">Error*: Please choose graph type</div>
        ) : null}
        <label>1) Type in Graph Name</label>
        <div>
          <input
            type="text"
            onChange={props.handleNameInput}
            name="graphName"
            value={props.nameInput}
            placeholder="My Awesome New Chart..."
            onKeyDown={(e) => enterNameInput(e)}
          />
          <button onClick={props.submitNameInput}>Submit</button>
        </div>
        <label>2) Data: Type in integers separated by commas</label>
        <div>
          <input
            type="text"
            onChange={props.handleGraphInput}
            name="graphData"
            value={props.rawGraphInput}
            placeholder="ie..1,2,3,4,5,7"
            onKeyDown={(e) => enterDataInput(e)}
          />
          <button onClick={props.submitRawGraphInput}>Generate Chart</button>
        </div>
        <label>3) Graph type?</label>
        <form>
          <input
            type="radio"
            value="bar"
            id="bar"
            onChange={props.handleGraphType}
            name="graphType"
          />
          <label for="bar">bar</label>
          <input
            type="radio"
            value="line"
            id="line"
            onChange={props.handleGraphType}
            name="graphType"
          />
          <label for="line">line</label>
        </form>
      </div>
    </div>
  );
};

export default UserInputs;
