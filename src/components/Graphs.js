import React from 'react';
import { LineChart, BarChart } from 'react-chartkick'

const Graphs = (props) => {
    return <div>
    <h2>{props.name}</h2>
      {props.formattedLineGInput !=='' && props.graphType=="line" ? <LineChart data={props.formattedLineGInput} 
      label="Value"
      xtitle="Item" 
      ytitle="Value"
      curve={false}
      /> : null}
       {props.formattedBarGInput !=='' && props.graphType=="bar" ?
      <BarChart data={props.formattedBarGInput} /> : null }
    </div>
}

export default Graphs