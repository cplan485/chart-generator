import "./App.css";
import React from "react";
import "chart.js";
import UserInputs from "./components/UserInputs.js";
import Graphs from "./components/Graphs.js";

class App extends React.Component {
  state = {
    nameInput: "",
    name: "",
    rawGraphInput: "",
    formattedLineGInput: "",
    formattedBarGInput: "",
    graphType: "",
  };

  //Functions to Handle User Inputs

  handleNameInput = (e) => {
    this.setState({ nameInput: e.target.value });
  };

  submitNameInput = () => {
    this.setState({ name: this.state.nameInput });
    this.setState({ nameInput: "" });
  };

  handleGraphInput = (e) => {
    this.setState({ rawGraphInput: e.target.value });
  };

  submitRawGraphInput = () => {
    let integersConvertedToArr = this.state.rawGraphInput.split(",");
    let lineDataObj = this.convertRawDataToLineGData(integersConvertedToArr);
    let barDataArr = this.convertRawDataToBarGData(integersConvertedToArr);
    this.setState({ formattedLineGInput: lineDataObj });
    this.setState({ formattedBarGInput: barDataArr });
  };

  //Functions to convert raw integers string to formats readable for Line and Bar Charts

  convertRawDataToLineGData = (arr) => {
    let lineGObj = {};
    arr.map((item, idx) => {
      let id = `Item_${idx + 1}`;
      lineGObj[id] = Number(item);
    });
    return lineGObj;
  };

  convertRawDataToBarGData = (arr) => {
    let lineBarArr = [];
    arr.map((item, idx) => {
      lineBarArr.push([`Item ${idx + 1}`, item]);
    });
    return lineBarArr;
  };
  //Toggle between bar and line graphs

  handleGraphType = (e) => {
    this.setState({
      graphType: e.target.value,
    });
  };

  render() {
    return (
      <div className="App">
        <UserInputs
          handleNameInput={this.handleNameInput}
          submitNameInput={this.submitNameInput}
          formattedLineGInput={this.state.formattedLineGInput}
          graphType={this.state.graphType}
          nameInput={this.state.nameInput}
          handleGraphInput={this.handleGraphInput}
          rawGraphInput={this.state.rawGraphInput}
          submitRawGraphInput={this.submitRawGraphInput}
          handleGraphType={this.handleGraphType}
        />
        <Graphs
          name={this.state.name}
          formattedLineGInput={this.state.formattedLineGInput}
          graphType={this.state.graphType}
          formattedBarGInput={this.state.formattedBarGInput}
        />
      </div>
    );
  }
}

export default App;
